/*
 * Fake VISA support for TL CCS devices.
 * 
 *  Created on: Jan 23, 2015
 *  Updated on: Nov 23, 2018
 *      Author: Hinko Kocevar for ESS ERIC
 *     License: MIT
 *
 * Goal is to simplify the VISA implementation in order to use vendor supplied
 * TL CCS NI VISA driver with EPICS, on Linux.
 *
 * We need to support at least the set of viXXX functions that tlccsdrv.c wants.
 *
 * We need to support multiple devices, meaning the same VID and PID, but
 * different serial number.
 *
 * We need to support firmware upload to TL CCS device that need it; all after 
 * connection to the system.
 * 
 * We need to maintain basic data structure to have viXXX functions quick access
 * to the USB device.
 *
 * See vendor folder for set of files coming from the vendor, including the C based
 * implementation used here (TLCCS.h and TLCCS.c).
 *
 */

#include <stdarg.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <libusb-1.0/libusb.h>

#define NIVISA_USB
#include "visa.h"

/* Firmware is bundled in header files generated with:
 *  $ xxd -i CCSxxx.spt CCSxxx_spt.h
 */
#include "CCS100_spt.h"
#include "CCS125_spt.h"
#include "CCS150_spt.h"
#include "CCS175_spt.h"
#include "CCS200_spt.h"

/*---------------------------------------------------------------------------
 Data structures
---------------------------------------------------------------------------*/
typedef struct {
    // data for USB device detection and (un)registration
    char rsrc[VI_FIND_BUFLEN];
    libusb_device *usbDev;
    libusb_device_handle *usbDevHandle;
    int usbDevClaimed;
    uint16_t usbDevVID;
    uint16_t usbDevPID;
    uint8_t usbDevBulkInEndpoint;

    // vi related data
    ViSession viSession;
    // accessed by the viSetAttribute/viGetAttribute
    // set up initially through the device detection
    ViAttrState userData;
    ViAttrState manfID;
    char manfName[VI_FIND_BUFLEN];
    ViAttrState modelCode;
    char modelName[VI_FIND_BUFLEN];
    ViAttrState tmoValue;
    char usbSerialNum[VI_FIND_BUFLEN];

} USBDevice;

#define MAX_USB_DEVICES        10
typedef struct {
    ViSession dmSession;
    USBDevice devices[MAX_USB_DEVICES];
} fakeViManager;

typedef struct {
    ViStatus err;
    ViString descr;
} ViErrorString;

/*---------------------------------------------------------------------------
 Globals
---------------------------------------------------------------------------*/
static fakeViManager manager = {0};
static const ViErrorString errorStrings[] =
{
    {VI_SUCCESS,                    "Operation completed successfully."},
    {VI_SUCCESS_EVENT_EN,           "Specified event is already enabled for at least one of the specified mechanisms."},
    {VI_SUCCESS_EVENT_DIS,          "Specified event is already disabled for at least one of the specified mechanisms."},
    {VI_SUCCESS_QUEUE_EMPTY,        "Operation completed successfully, but queue was already empty."},
    {VI_SUCCESS_TERM_CHAR,          "The specified termination character was read."},
    {VI_SUCCESS_MAX_CNT,            "The number of bytes transferred is equal to the requested input count. More data may be available."},
    {VI_SUCCESS_DEV_NPRESENT,       "Session opened successfully, but the device at the specified address is not responding."},
    {VI_SUCCESS_TRIG_MAPPED,        "The path from trigSrc to trigDest is already mapped."},
    {VI_SUCCESS_QUEUE_NEMPTY,       "Wait terminated successfully on receipt of an event notification. There is at least one more event object of the requested type(s) available for this session."},
    {VI_SUCCESS_NCHAIN,             "Event handled successfully. Do not invoke any other handlers on this session for this event."},
    {VI_SUCCESS_NESTED_SHARED,      "Operation completed successfully, and this session has nested shared locks."},
    {VI_SUCCESS_NESTED_EXCLUSIVE,   "Operation completed successfully, and this session has nested exclusive locks."},
    {VI_SUCCESS_SYNC,               "Operation completed successfully, but the operation was actually synchronous rather than asynchronous."},
    {VI_WARN_QUEUE_OVERFLOW,        "VISA received more event information of the specified type than the configured queue size could hold."},
    {VI_WARN_CONFIG_NLOADED,        "The specified configuration either does not exist or could not be loaded. VISA-specified defaults will be used."},
    {VI_WARN_NULL_OBJECT,           "The specified object reference is uninitialized."},
    {VI_WARN_NSUP_ATTR_STATE,       "Although the specified state of the attribute is valid, it is not supported by this implementation."},
    {VI_WARN_UNKNOWN_STATUS,        "The status code passed to the operation could not be interpreted."},
    {VI_WARN_NSUP_BUF,              "The specified I/O buffer type is not supported."},
    {VI_WARN_EXT_FUNC_NIMPL,        "The operation succeeded, but a lower level driver did not implement the extended functionality."},
    {VI_ERROR_SYSTEM_ERROR,         "Unknown system error (miscellaneous error)."},
    {VI_ERROR_INV_OBJECT,           "The given session or object reference is invalid."},
    {VI_ERROR_RSRC_LOCKED,          "Specified type of lock cannot be obtained, or specified operation cannot be performed, because the resource is locked."},
    {VI_ERROR_INV_EXPR,             "Invalid expression specified for search."},
    {VI_ERROR_RSRC_NFOUND,          "Insufficient location information or the requested device or resource is not present in the system."},
    {VI_ERROR_INV_RSRC_NAME,        "Invalid resource reference specified. Parsing error."},
    {VI_ERROR_INV_ACC_MODE,         "Invalid access mode."},
    {VI_ERROR_TMO,                  "Timeout expired before operation completed."},
    {VI_ERROR_CLOSING_FAILED,       "The VISA driver failed to properly close the session or object reference. This might be due to an error freeing internal or OS resources, a failed network connection, or a lower-level driver or OS error."},
    {VI_ERROR_INV_DEGREE,           "Specified degree is invalid."},
    {VI_ERROR_INV_JOB_ID,           "Specified job identifier is invalid."},
    {VI_ERROR_NSUP_ATTR,            "The specified attribute is not defined or supported by the referenced object."},
    {VI_ERROR_NSUP_ATTR_STATE,      "The specified state of the attribute is not valid, or is not supported as defined by the object."},
    {VI_ERROR_ATTR_READONLY,        "The specified attribute is read-only."},
    {VI_ERROR_INV_LOCK_TYPE,        "The specified type of lock is not supported by this resource."},
    {VI_ERROR_INV_ACCESS_KEY,       "The access key to the resource associated with the specified session is invalid."},
    {VI_ERROR_INV_EVENT,            "Specified event type is not supported by the resource."},
    {VI_ERROR_INV_MECH,             "Invalid mechanism specified."},
    {VI_ERROR_HNDLR_NINSTALLED,     "A handler was not installed."},
    {VI_ERROR_INV_HNDLR_REF,        "The given handler reference is either invalid or was not installed."},
    {VI_ERROR_INV_CONTEXT,          "Specified event context is invalid."},
    {VI_ERROR_QUEUE_OVERFLOW,       "The event queue for the specified type has overflowed (usually due to previous events not having been closed)."},
    {VI_ERROR_NENABLED,             "You must be enabled for events of the specified type in order to receive them."},
    {VI_ERROR_ABORT,                "User abort occurred during transfer."},
    {VI_ERROR_RAW_WR_PROT_VIOL,     "Violation of raw write protocol occurred during transfer."},
    {VI_ERROR_RAW_RD_PROT_VIOL,     "Violation of raw read protocol occurred during transfer."},
    {VI_ERROR_OUTP_PROT_VIOL,       "Device reported an output protocol error during transfer."},
    {VI_ERROR_INP_PROT_VIOL,        "Device reported an input protocol error during transfer."},
    {VI_ERROR_BERR,                 "Bus error occurred during transfer."},
    {VI_ERROR_IN_PROGRESS,          "Unable to queue the asynchronous operation because there is already an operation in progress."},
    {VI_ERROR_INV_SETUP,            "Unable to start operation because setup is invalid (usually due to attributes being set to an inconsistent state)."},
    {VI_ERROR_QUEUE_ERROR,          "Unable to queue the asynchronous operation (usually due to the I/O completion event not being enabled or insufficient space in the session's queue)."},
    {VI_ERROR_ALLOC,                "Insufficient system resources to perform necessary memory allocation."},
    {VI_ERROR_INV_MASK,             "Invalid buffer mask specified."},
    {VI_ERROR_IO,                   "Could not perform operation because of I/O error."},
    {VI_ERROR_INV_FMT,              "A format specifier in the format string is invalid."},
    {VI_ERROR_NSUP_FMT,             "A format specifier in the format string is not supported."},
    {VI_ERROR_LINE_IN_USE,          "The specified trigger line is currently in use."},
    {VI_ERROR_NSUP_MODE,            "The specified mode is not supported by this VISA implementation."},
    {VI_ERROR_SRQ_NOCCURRED,        "Service request has not been received for the session."},
    {VI_ERROR_INV_SPACE,            "Invalid address space specified."},
    {VI_ERROR_INV_OFFSET,           "Invalid offset specified."},
    {VI_ERROR_INV_WIDTH,            "Invalid access width specified."},
    {VI_ERROR_NSUP_OFFSET,          "Specified offset is not accessible from this hardware."},
    {VI_ERROR_NSUP_VAR_WIDTH,       "Cannot support source and destination widths that are different."},
    {VI_ERROR_WINDOW_NMAPPED,       "The specified session is not currently mapped."},
    {VI_ERROR_RESP_PENDING,         "A previous response is still pending, causing a multiple query error."},
    {VI_ERROR_NLISTENERS,           "No listeners condition is detected (both NRFD and NDAC are deasserted)."},
    {VI_ERROR_NCIC,                 "The interface associated with this session is not currently the controller in charge."},
    {VI_ERROR_NSYS_CNTLR,           "The interface associated with this session is not the system controller."},
    {VI_ERROR_NSUP_OPER,            "The given session or object reference does not support this operation."},
    {VI_ERROR_INTR_PENDING,         "An interrupt is still pending from a previous call."},
    {VI_ERROR_ASRL_PARITY,          "A parity error occurred during transfer."},
    {VI_ERROR_ASRL_FRAMING,         "A framing error occurred during transfer."},
    {VI_ERROR_ASRL_OVERRUN,         "An overrun error occurred during transfer. A character was not read from the hardware before the next character arrived."},
    {VI_ERROR_TRIG_NMAPPED,         "The path from trigSrc to trigDest is not currently mapped."},
    {VI_ERROR_NSUP_ALIGN_OFFSET,    "The specified offset is not properly aligned for the access width of the operation."},
    {VI_ERROR_USER_BUF,             "A specified user buffer is not valid or cannot be accessed for the required size."},
    {VI_ERROR_RSRC_BUSY,            "The resource is valid, but VISA cannot currently access it."},
    {VI_ERROR_NSUP_WIDTH,           "Specified width is not supported by this hardware."},
    {VI_ERROR_INV_PARAMETER,        "The value of some parameter (which parameter is not known) is invalid."},
    {VI_ERROR_INV_PROT,             "The protocol specified is invalid."},
    {VI_ERROR_INV_SIZE,             "Invalid size of window specified."},
    {VI_ERROR_WINDOW_MAPPED,        "The specified session currently contains a mapped window."},
    {VI_ERROR_NIMPL_OPER,           "The given operation is not implemented."},
    {VI_ERROR_INV_LENGTH,           "Invalid length specified."},
    {VI_ERROR_INV_MODE,             "Invalid mode specified."},
    {VI_ERROR_SESN_NLOCKED,         "The current session did not have a lock on the resource."},
    {VI_ERROR_MEM_NSHARED,          "The device does not export any memory."},
    {VI_ERROR_LIBRARY_NFOUND,       "A code library required by VISA could not be located or loaded."},
    {VI_ERROR_NSUP_INTR,            "The interface cannot generate an interrupt on the requested level or with the requested statusID value."},
    {VI_ERROR_INV_LINE,             "The value specified by the line parameter is invalid."},
    {VI_ERROR_FILE_ACCESS,          "An error occurred while trying to open the specified file. Possible reasons include an invalid path or lack of access rights."},
    {VI_ERROR_FILE_IO,              "An error occurred while performing I/O on the specified file."},
    {VI_ERROR_NSUP_LINE,            "One of the specified lines (trigSrc or trigDest) is not supported by this VISA implementation, or the combination of lines is not a valid mapping."},
    {VI_ERROR_NSUP_MECH,            "The specified mechanism is not supported for the given event type."},
    {VI_ERROR_INTF_NUM_NCONFIG,     "The interface type is valid but the specified interface number is not configured."},
    {VI_ERROR_CONN_LOST,            "The connection for the given session has been lost."},
    {VI_ERROR_MACHINE_NAVAIL,       "The remote machine does not exist or is not accepting any connections. If the NI-VISA server is installed and running on the remote machine, it may have an incompatible version or may be listening on a different port."},
    {VI_ERROR_NPERMISSION,          "Access to the resource or remote machine is denied. This is due to lack of sufficient privileges for the current user or machine"},
    {VI_USB_PIPE_STATE_UNKNOWN,     "USB pipe status is unknown."}
};

/*---------------------------------------------------------------------------
 Debug
---------------------------------------------------------------------------*/
#define ERR(format, ...) \
    fprintf(stderr, \
            "ERR %s:%d:%s(): " \
            format, \
            __FILE__, \
            __LINE__, \
            __func__, \
            ## __VA_ARGS__)

#define INF(format, ...) \
    do { if (1) \
        fprintf(stderr, \
                "INF %s:%d:%s(): " \
                format, \
                __FILE__, \
                __LINE__, \
                __func__, \
                ## __VA_ARGS__); \
    } while (0)

#define DBG(format, ...) \
    do { if (_DBG) \
        fprintf(stderr, \
                "DBG %s:%d:%s(): " \
                format, \
                __FILE__, \
                __LINE__, \
                __func__, \
                ## __VA_ARGS__); \
    } while (0)

#define ASSERT(x) \
    do { if (_DBG) \
        assert(x); \
    } while(0)

#define E       DBG("Enter >>>\n");
#define L       DBG("Leave <<<\n");
#define R(x)    { DBG("Leave <<<\n"); return (x); }

/*---------------------------------------------------------------------------
 USB VIDs and PIDs
---------------------------------------------------------------------------*/
#define THORLABS_VID      0x1313        /* Thorlabs */
#define CCS100_PID        0x8081        /* CCS100 Compact Spectrometer */
#define CCS125_PID        0x8083        /* CCS125 Special Spectrometer */
#define CCS150_PID        0x8085        /* CCS150 UV Spectrometer */
#define CCS175_PID        0x8087        /* CCS175 NIR Spectrometer */
#define CCS200_PID        0x8089        /* CCS200 UV-NIR Spectrometer */

/*---------------------------------------------------------------------------
 Prototypes
---------------------------------------------------------------------------*/
static int findDevice(ViRsrc rsrc, ViPSession vi);
static int isDevicePresent(ViRsrc rsrc);
static int findDeviceWithoutFirmware(USBDevice *dev);
static int findDeviceWithFirmware(USBDevice *dev);
static int uploadDeviceFirmware(USBDevice *dev);
static int registerDevice(USBDevice *dev);
static int unregisterDevice(USBDevice *dev);
static int getDeviceStrings(USBDevice *dev);
static int openDevice(USBDevice *dev);
static int closeDevice(USBDevice *dev);
static int isDeviceFree(USBDevice *dev);
static USBDevice *getDevice(ViSession vi);
static int xferDeviceControl(USBDevice *dev,
        uint8_t bmRequestType,
        uint8_t bRequest,
        uint16_t wValue,
        uint16_t wIndex,
        unsigned char *data,
        uint16_t wLength,
        unsigned int timeout);
static int xferDeviceBulk(USBDevice *dev,
        unsigned char *data,
        int wLength,
        int *wActualLength,
        unsigned int timeout);
static ViUInt32 RSHash(const ViPChar str, ViUInt32 length);
static void hexdump(FILE * stream, void const * data, unsigned int len);

/*---------------------------------------------------------------------------
 VI functions (public)
---------------------------------------------------------------------------*/

/**
 * From http://zone.ni.com/reference/en-XX/help/370131S-01/ni-visa/visa_resource_manager/
 *
 * VISA Resource Manager
 *
 * This section lists the attributes, events, and operations for the VISA
 * Resource Manager. The attributes, events, and operations in the VISA
 * Resource Template are available to this resource in addition to the
 * operations listed below.
 *
 * Attributes
 *
 * The attributes for the VISA Resource Template are available to this
 * resource. This resource has no defined attributes of its own.
 *
 * Events
 *
 * None
 *
 * Operations
 *
 * viFindNext (findList, instrDesc)
 * viFindRsrc (sesn, expr, findList, retcnt, instrDesc)
 * viOpen (sesn, rsrcName, accessMode, timeout, vi)
 * viOpenDefaultRM (sesn)
 * viParseRsrc(sesn, rsrcName, intfType, intfNum)
 * viParseRsrcEx (sesn, rsrcName, intfType, intfNum, rsrcClass, unaliasdExpandedRsrcName, aliasIfExists)
 *
 * HK
 * We will implement limited and simplified set of above operations to
 * mimic VISA Resource Manager in order to support TL CCS operation.
 */

/**
 * From http://zone.ni.com/reference/en-XX/help/370131S-01/ni-visa/viopendefaultrm/
 *
 * The viOpenDefaultRM() function must be called before any VISA operations
 * can be invoked. The first call to this function initializes the VISA
 * system, including the Default Resource Manager resource, and also returns
 * a session to that resource. Subsequent calls to this function return unique
 * sessions to the same Default Resource Manager resource.
 *
 * When a Resource Manager session is passed to viClose(), not only is that
 * session closed, but also all find lists and device sessions (which that
 * Resource Manager session was used to create) are closed.
 */
ViStatus _VI_FUNC  viOpenDefaultRM (ViPSession vi) {
    // HK
    // 
    // NOTE
    // We create a library wide 'default resource manager' storage/variable.
    // Each application that links with this library will get its own copy of
    // the 'default resource manager' storage/variable.
    // Application needs to make sure it uses free TL CCS device; no
    // detection against two users of the same USB device is made here!
    // 
    // first function usually called for VISA based control!
    // called in tlccs_init(), right before viOpen()
    // closed in tlccs_initClose() if VI_ATTR_RM_SESSION attribute was
    // previously set; do it here!
    // 
    // must open libusb..
    // allocate structures for holding information for TL CCS devices
    // handle devices before and after firmware upload
    // automatically upload firmware to TL CCS 'vanilla' devices:
    //  for n in [0,2,4,6,8]
    //    converts 0x1313::0x808[n] to 0x1313::0x808[n+1]
    // TL CCS USB devices with firmware:
    //  detect TL CCS device and read serial number
    //  save a map 'USB::0x1313::PID::SERIAL::RAW' to libusb_device struct
    //  
    // viOpen() shall request a known 'USB::0x1313::PID::SERIAL::RAW' resource
    // and we shall not support any searching or similar ; no viFindRsrc()
    // or viFindNext()
    // 
    // ? how to 'better' handle multiple accesses to the same device ?
    // ? set VI_ATTR_RM_SESSION attribute; used in tlccs_initClose() ?

    E

    if (vi == NULL) {
        ERR("Invalid vi ViObject\n");
        R(VI_ERROR_INV_OBJECT)
    }

    /* initialize resource manager resource once */
    if (manager.dmSession == 0) {
        const ViPChar s = "@ResourceManager";
        ViUInt32 hash = RSHash(s, strlen(s));
        manager.dmSession = hash;
        DBG("resource manager session 0x%08X\n", manager.dmSession);
        int r = libusb_init(NULL);
        if (r) {
            ERR("libusb_init failed: %s!\n", libusb_error_name(r));
            R(VI_ERROR_SYSTEM_ERROR)
        }
        libusb_set_debug(NULL, LIBUSB_LOG_LEVEL_ERROR);
    }

    ASSERT(manager.dmSession != 0);

    /* return new session to resource manager */
    *vi = manager.dmSession;

    R(VI_SUCCESS);
}

//
// XXX use viClose() and VI_ATTR_RM_SESSION attribute through tlccs_initClose()
//     instead
#if 0
/**
 * This is not part of VISA.
 * Used to call viClose() on resource manager resource ViSession.
 * It will deallocate all resources and sessions and call usbExit().
 */
ViStatus _VI_FUNC  viCLoseDefaultRM () {
    // HK
    // should close libusb
    // ? handle VI_ATTR_RM_SESSION destroy here ?
    // viGetAttribute(vi, VI_ATTR_RM_SESSION, &rm);

    return VI_ERROR_SYSTEM_ERROR;
}
#endif

/**
 * From http://zone.ni.com/reference/en-XX/help/370131S-01/ni-visa/vifindrsrc/
 *
 * The viFindRsrc() operation matches the value specified in the expr parameter
 * with the resources available for a particular interface. A regular
 * expression is a string consisting of ordinary characters as well as
 * special characters. You use a regular expression to specify patterns to
 * match in a given string; in other words, it is a search criterion.
 *
 * On successful completion, this function returns the first resource found
 * (instrDesc) and returns a count (retCnt) to indicate if there were more
 * resources found for the designated interface. This function also returns,
 * in the findList parameter, a handle to a find list. This handle points to
 * the list of resources and it must be used as an input to viFindNext().
 * When this handle is no longer needed, it should be passed to viClose().
 * Notice that retCnt and findList are optional parameters. This is useful
 * if only the first match is important, and the number of matches is not
 * needed. If you specify VI_NULL in the findList parameter and the
 * operation completes successfully, VISA automatically invokes viClose()
 * on the find list handle rather than returning it to the application.
 */
ViStatus _VI_FUNC  viFindRsrc      (ViSession sesn, ViString expr, ViPFindList findList,
                                    ViPUInt32 retCnt, ViChar _VI_FAR instrDesc[]) {

    // HK
    // we should not need to invoke viFindRsrc() or viFindNext() to
    // find the device, since we MUST know its resource to start with (EPICS)!
    E
    R(VI_ERROR_SYSTEM_ERROR);
}

/**
 * From http://zone.ni.com/reference/en-XX/help/370131S-01/ni-visa/vifindnext/
 *
 * The viFindNext() operation returns the next device found in the list
 * created by viFindRsrc(). The list is referenced by the handle that was
 * returned by viFindRsrc().
 */
ViStatus _VI_FUNC  viFindNext      (ViFindList findList, ViChar _VI_FAR instrDesc[]) {
	// HK
    // we should not need to invoke viFindRsrc() or viFindNext() to
    // find the device, since we MUST know its resource to start with (EPICS)!
    E
    R(VI_ERROR_SYSTEM_ERROR);
}

/**
 * From http://zone.ni.com/reference/en-XX/help/370131S-01/ni-visa/viopen/
 *
 * The viOpen() operation opens a session to the specified resource. It
 * returns a session identifier that can be used to call any other operations
 * of that resource. The address string passed to viOpen() must uniquely
 * identify a resource. Refer to VISA Resource Syntax and Examples for the
 * syntax of resource strings and examples.
 *
 * For the parameter accessMode, the value VI_EXCLUSIVE_LOCK (1) is used to
 * acquire an exclusive lock immediately upon opening a session; if a lock
 * cannot be acquired, the session is closed and an error is returned.
 * The value VI_LOAD_CONFIG (4) is used to configure attributes to values
 * specified by some external configuration utility. Multiple access modes
 * can be used simultaneously by specifying a bit-wise OR of the values other
 * than VI_NULL. NI-VISA currently supports VI_LOAD_CONFIG only on Serial
 * INSTR sessions.
 *
 * All resource strings returned by viFindRsrc() will always be recognized by
 * viOpen(). However, viFindRsrc() will not necessarily return all strings
 * that you can pass to viParseRsrc() or viOpen(). This is especially true
 * for network and TCPIP resources.
 */
ViStatus _VI_FUNC  viOpen          (ViSession sesn, ViRsrc name, ViAccessMode mode,
                                    ViUInt32 timeout, ViPSession vi) {
    // HK
    // lookup the resource name in the list of resources created in viOpenDefaultRM
    // if found return success
    // if not found fail
    // For USB RAW sessions,set the endpoints referred to by the
    // attributes VI_ATTR_USB_BULK_IN_PIPE and VI_ATTR_USB_BULK_OUT_PIPE.
    E

    int r = isDevicePresent(name);
    if (r) {
        R(VI_ERROR_RSRC_LOCKED);
    }

    r = findDevice(name, vi);
    if (r < 0) {
        R(r);
    }

    if (*vi == 0) {
        R(VI_ERROR_RSRC_NFOUND);
    }

    R(VI_SUCCESS);
}

/**
 * From http://zone.ni.com/reference/en-XX/help/370131S-01/ni-visa/viclose/
 *
 * The viClose() operation closes a session, event, or a find list. In this
 * process all the data structures that had been allocated for the specified
 * vi are freed. Calling viClose() on a VISA Resource Manager session will
 * also close all I/O sessions associated with that resource manager session.
 */
ViStatus _VI_FUNC  viClose         (ViObject vi) {
    // HK
    // close the USB device if still opened

    E

    if (vi == 0) {
        ERR("Not a session ViObject\n");
        R(VI_ERROR_INV_OBJECT);
    }

    if (manager.dmSession == vi) {
        libusb_exit(NULL);
    } else {
        USBDevice *dev = getDevice(vi);
        ASSERT(dev != NULL);
        unregisterDevice(dev);
    }

    R(VI_SUCCESS);
}

/**
 * From http://zone.ni.com/reference/en-XX/help/370131S-01/ni-visa/visetattribute/
 *
 * The viSetAttribute() operation is used to modify the state of an attribute
 * for the specified object.
 *
 * Both VI_WARN_NSUP_ATTR_STATE and VI_ERROR_NSUP_ATTR_STATE indicate that
 * the specified attribute state is not supported. A resource normally
 * returns the error code VI_ERROR_NSUP_ATTR_STATE when it cannot set a
 * specified attribute state. The completion code VI_WARN_NSUP_ATTR_STATE
 * is intended to alert the application that although the specified optional
 * attribute state is not supported, the application should not fail.
 * One example is attempting to set an attribute value that would increase
 * performance speeds. This is different than attempting to set an attribute
 * value that specifies required but nonexistent hardware (such as specifying
 * a VXI ECL trigger line when no hardware support exists) or a value
 * that would change assumptions a resource might make about the way
 * data is stored or formatted (such as byte order).
 */
ViStatus _VI_FUNC  viSetAttribute  (ViObject vi, ViAttr attrName, ViAttrState attrValue) {
    // HK
    // sets any of the attribute the TL CCS needs in the local attribute store

    if (vi == 0) {
        ERR("Not a session ViObject\n");
        R(VI_ERROR_INV_OBJECT);
    }
    USBDevice *dev = getDevice(vi);
    ASSERT(dev != NULL);

    /* XXX: Are there any other attributes to take care of? */
    switch (attrName) {
    case VI_ATTR_USER_DATA:
        dev->userData = attrValue;
        break;
    case VI_ATTR_RM_SESSION:
        manager.dmSession = attrValue;
        break;
    case VI_ATTR_MANF_ID:
        dev->manfID = attrValue;
        break;
    case VI_ATTR_MANF_NAME:
        strncpy(dev->manfName, (char *)attrValue, VI_FIND_BUFLEN);
        break;
    case VI_ATTR_MODEL_CODE:
        dev->modelCode = attrValue;
        break;
    case VI_ATTR_MODEL_NAME:
        strncpy(dev->modelName, (char *)attrValue, VI_FIND_BUFLEN);
        break;
    case VI_ATTR_USB_SERIAL_NUM:
        strncpy(dev->usbSerialNum, (char *)attrValue, VI_FIND_BUFLEN);
        break;
    case VI_ATTR_TMO_VALUE:
        dev->tmoValue = attrValue;
        break;
    default:
        R(VI_ERROR_NSUP_ATTR);
    }

    return VI_SUCCESS;
}

/**
 * From http://zone.ni.com/reference/en-XX/help/370131S-01/ni-visa/vigetattribute/
 *
 * The viGetAttribute() operation is used to retrieve the state of an
 * attribute for the specified session, event, or find list.
 *
 * The output parameter attrState is of the type of the attribute actually
 * being retrieved. For example, when retrieving an attribute that is defined
 * as a ViBoolean, your application should pass a reference to a variable of
 * type ViBoolean. Similarly, if the attribute is defined as being ViUInt32,
 * your application should pass a reference to a variable of type ViUInt32.
 */
ViStatus _VI_FUNC  viGetAttribute  (ViObject vi, ViAttr attrName, void _VI_PTR attrValue) {
    // HK
    // gets any of the attribute the TL CCS needs from the local attribute store

    if (vi == 0) {
        ERR("Not a session ViObject\n");
        R(VI_ERROR_INV_OBJECT);
    }
    USBDevice *dev = getDevice(vi);
    ASSERT(dev != NULL);

    /* XXX: Are there any other attributes to take care of? */
    switch (attrName) {
    case VI_ATTR_USER_DATA:
        *(ViAttrState *)attrValue = dev->userData;
        break;
    case VI_ATTR_RM_SESSION:
        *(ViAttrState *)attrValue = manager.dmSession;
        break;
    case VI_ATTR_MANF_ID:
        *(ViUInt16 *)attrValue = dev->manfID;
        break;
    case VI_ATTR_MANF_NAME:
        strncpy((char *)attrValue, dev->manfName, VI_FIND_BUFLEN);
        break;
    case VI_ATTR_MODEL_CODE:
        *(ViUInt16 *)attrValue = dev->modelCode;
        break;
    case VI_ATTR_MODEL_NAME:
        strncpy((char *)attrValue, dev->modelName, VI_FIND_BUFLEN);
        break;
    case VI_ATTR_USB_SERIAL_NUM:
        strncpy((char *)attrValue, dev->usbSerialNum, VI_FIND_BUFLEN);
        break;
    case VI_ATTR_TMO_VALUE:
        *(ViAttrState *)attrValue = dev->tmoValue;
        break;
    default:
        R(VI_ERROR_NSUP_ATTR);
    }

    return VI_SUCCESS;
}

/**
 * From http://zone.ni.com/reference/en-XX/help/370131S-01/ni-visa/vistatusdesc/
 *
 * The viStatusDesc() operation is used to retrieve a user-readable string
 * that describes the status code presented. If the string cannot be
 * interpreted, the operation returns the warning code VI_WARN_UNKNOWN_STATUS.
 * However, the output string desc is valid regardless of the status
 * return value.
 *
 * Note: The size of the desc parameter should be at least 256 bytes.
 */
ViStatus _VI_FUNC  viStatusDesc    (ViObject vi, ViStatus status, ViChar _VI_FAR desc[]) {
    // HK
    // called in tlccs_error_message() to translate the error code into string message
    if (!desc) {
        R(VI_ERROR_INV_PARAMETER);
    }

    /* VISA errors */
    const ViErrorString *ptr = errorStrings;
    while (ptr->descr != VI_NULL) {
        if (ptr->err == status) {
            strcpy(desc, ptr->descr);
            return VI_SUCCESS;
        }
        ptr++;
    }
    R(VI_WARN_UNKNOWN_STATUS);
}

/**
 * From http://zone.ni.com/reference/en-XX/help/370131S-01/ni-visa/viread/
 *
 * The viRead() operation synchronously transfers data. The data read is
 * to be stored in the buffer represented by buf. This operation
 * returns only when the transfer terminates. Only one synchronous read
 * operation can occur at any one time.
 *
 * Note: The retCount and buf parameters always are valid on both success
 * and error.
 */
ViStatus _VI_FUNC  viRead          (ViSession vi, ViPBuf buf, ViUInt32 cnt, ViPUInt32 retCnt) {
    // HK
    // called from tlccs_USB_read()
    // performs USB bulk in read
    
    if (vi == 0) {
        ERR("Not a session ViObject\n");
        R(VI_ERROR_INV_OBJECT);
    }
    USBDevice *dev = getDevice(vi);
    ASSERT(dev != NULL);

    int r = xferDeviceBulk(dev, buf, cnt, (int *)retCnt, dev->tmoValue);
    if (r < 0) {
        R(VI_USB_PIPE_STATE_UNKNOWN);
    }
    return VI_SUCCESS;
}

/**
 * From http://zone.ni.com/reference/en-XX/help/370131S-01/ni-visa/viclear/
 *
 * Clear for Non-488.2 Instruments (Serial INSTR, TCPIP SOCKET, and USB RAW)
 *
 * For Serial INSTR sessions, VISA flushes (discards) the I/O output buffer,
 *  sends a break, and then flushes (discards) the I/O input buffer.
 * For TCPIP SOCKET sessions, VISA flushes (discards) the I/O buffers.
 * For USB RAW sessions, VISA resets the endpoints referred to by the
 *  attributes VI_ATTR_USB_BULK_IN_PIPE and VI_ATTR_USB_BULK_OUT_PIPE.
 *
 *  Invoking viClear() also discards the read and write buffers used
 *  by the formatted I/O services for that session.
 */
ViStatus _VI_FUNC  viClear         (ViSession vi) {
    // HK
    // do something here? 
    
    if (vi == 0) {
        ERR("Not a session ViObject\n");
        R(VI_ERROR_INV_OBJECT);
    }
    USBDevice *dev = getDevice(vi);
    ASSERT(dev != NULL);

    int r = libusb_reset_device(dev->usbDevHandle);
    if (r < 0) {
        R(VI_USB_PIPE_STATE_UNKNOWN);
    }
    return VI_SUCCESS;
}

/**
 * From http://zone.ni.com/reference/en-XX/help/370131S-01/ni-visa/viflush/
 *
 * It is possible to combine any of these read flags and write flags for
 * different buffers by ORing the flags. However, combining two flags for
 * the same buffer in the same call to viFlush() is illegal.
 *
 * Notice that when using formatted I/O operations with a session to a
 * Serial device or Ethernet socket, a flush of the formatted I/O buffers
 * also causes the corresponding I/O communication buffers to be flushed.
 * For example, calling viFlush() with VI_WRITE_BUF also flushes the
 * VI_IO_OUT_BUF.
 */
ViStatus _VI_FUNC  viFlush         (ViSession vi, ViUInt16 mask) {
    // HK
    // do something here? 
    return VI_SUCCESS;
}

/**
 * From http://zone.ni.com/reference/en-XX/help/370131S-01/ni-visa/viusbcontrolout/
 *
 * The viUsbControlOut() operation synchronously performs a USB control
 * pipe transfer to the device. The values of the data payload in the setup
 * stage of the control transfer are taken as parameters and include
 * bmRequestType, bRequest, wValue, wIndex, and wLength. An optional data
 * buffer buf contains the data to send if a data stage is required for
 * this transfer. Only one USB control pipe transfer operation can occur
 * at any one time.
 */
ViStatus _VI_FUNC  viUsbControlOut (ViSession vi, ViInt16 bmRequestType, ViInt16 bRequest,
                                    ViUInt16 wValue, ViUInt16 wIndex, ViUInt16 wLength,
                                    ViBuf buf) {
    // called from tlccs_USB_out()
    // performs USB control write device access
    
    if (vi == 0) {
        ERR("Not a session ViObject\n");
        R(VI_ERROR_INV_OBJECT);
    }
    USBDevice *dev = getDevice(vi);
    ASSERT(dev != NULL);

    int r = xferDeviceControl(dev, bmRequestType, bRequest,
            wValue, wIndex, buf, wLength, dev->tmoValue);
    if (r < 0) {
        R(VI_USB_PIPE_STATE_UNKNOWN);
    }
    return VI_SUCCESS;
}

/**
 * From http://zone.ni.com/reference/en-XX/help/370131S-01/ni-visa/viusbcontrolin/
 *
 * The viUsbControlIn() operation synchronously performs a USB control pipe
 * transfer from the device. The values of the data payload in the setup
 * stage of the control transfer are taken as parameters and include
 * bmRequestType, bRequest, wValue, wIndex, and wLength. An optional data
 * buffer buf receives data if a data stage is required for this transfer.
 * Only one USB control pipe transfer operation can occur at any one time.
 */
ViStatus _VI_FUNC  viUsbControlIn  (ViSession vi, ViInt16 bmRequestType, ViInt16 bRequest,
                                    ViUInt16 wValue, ViUInt16 wIndex, ViUInt16 wLength,
                                    ViPBuf buf, ViPUInt16 retCnt) {
    // called from tlccs_USB_in()
    // performs USB control read device access

    if (vi == 0) {
        ERR("Not a session ViObject\n");
        R(VI_ERROR_INV_OBJECT);
    }
    USBDevice *dev = getDevice(vi);
    ASSERT(dev != NULL);

    int r = xferDeviceControl(dev, bmRequestType, bRequest,
            wValue, wIndex, buf, wLength, dev->tmoValue);
    if (r < 0) {
        R(VI_USB_PIPE_STATE_UNKNOWN);
    }
    *retCnt = r;
    return VI_SUCCESS;
}


/*---------------------------------------------------------------------------
 Private functions
---------------------------------------------------------------------------*/

static int isDevicePresent(ViRsrc rsrc) {
    USBDevice *d;
    for (int i = 0; i < MAX_USB_DEVICES; i++) {
        d = &manager.devices[i];
        if (strncmp(rsrc, d->rsrc, VI_FIND_BUFLEN) == 0) {
            return 1;
        }
    }
    return 0;
}

static int isDeviceFree(USBDevice *dev) {
    USBDevice *d;
    for (int i = 0; i < MAX_USB_DEVICES; i++) {
        d = &manager.devices[i];
        if (strncmp(dev->rsrc, d->rsrc, VI_FIND_BUFLEN) == 0) {
            return 0;
        }
    }
    return 1;
}

static USBDevice *getDevice(ViSession vi) {
    USBDevice *d;
    for (int i = 0; i < MAX_USB_DEVICES; i++) {
        d = &manager.devices[i];
        if (vi == d->viSession) {
            return d;
        }
    }
    return NULL;
}

static USBDevice *getFreeDevice() {
    USBDevice *d;
    for (int i = 0; i < MAX_USB_DEVICES; i++) {
        d = &manager.devices[i];
        if (d->viSession == 0) {
            return d;
        }
    }
    return NULL;
}

static int findDevice(ViRsrc rsrc, ViPSession vi) {
    int pid, vid;
    char serial[VI_FIND_BUFLEN];
    int r = sscanf(rsrc, "USB::0x%04X::0x%04X::%[^:]::RAW", &vid, &pid, serial);
    if (r != 3) {
        ERR("Invalid resource string '%s'", rsrc);
        R(VI_ERROR_INV_RSRC_NAME);
    }

    USBDevice tmpDev = {0};
    tmpDev.usbDevVID = vid;
    tmpDev.usbDevPID = pid - 1;
    r = findDeviceWithoutFirmware(&tmpDev);
    if ((r < 0) && (r != VI_ERROR_RSRC_NFOUND)) {
        R(r);
    }
    if (r > 0) {
        sleep(5);
    }

    tmpDev.usbDevVID = vid;
    tmpDev.usbDevPID = pid;
    strncpy(tmpDev.rsrc, rsrc, VI_FIND_BUFLEN);
    r = findDeviceWithFirmware(&tmpDev);
    if (r < 0) {
        R(r);
    }

    USBDevice *dev = getFreeDevice();
    ASSERT(dev != NULL);
    memcpy(dev, &tmpDev, sizeof(USBDevice));
    *vi = dev->viSession;

    return VI_SUCCESS;
}

static int findDeviceWithoutFirmware(USBDevice *dev) {
    DBG("Looking for device %04x:%04x\n", dev->usbDevVID, dev->usbDevPID);

    libusb_device **devs;
    ssize_t cnt = libusb_get_device_list(NULL, &devs);
    if (cnt < 0) {
        ERR("libusb_get_device_list failed: %s!\n", libusb_error_name(cnt));
        R(VI_ERROR_SYSTEM_ERROR);
    }

    libusb_device *usbDev;
    int i = 0;
    int status = VI_ERROR_RSRC_NFOUND;
    while ((usbDev = devs[i++]) != NULL) {
        struct libusb_device_descriptor desc;
        int r = libusb_get_device_descriptor(usbDev, &desc);
        if (r < 0) {
            ERR("libusb_get_device_descriptor failed: %s!\n", libusb_error_name(cnt));
            status = VI_ERROR_SYSTEM_ERROR;
            break;
        }

        DBG("%04x:%04x (bus %d, device %d)\n",
            desc.idVendor, desc.idProduct,
            libusb_get_bus_number(usbDev), libusb_get_device_address(usbDev));

        if ((desc.idVendor == dev->usbDevVID) && (desc.idProduct == dev->usbDevPID)) {
            INF("Found device %04x:%04x\n", dev->usbDevVID, dev->usbDevPID);
            dev->usbDev = usbDev;
            status = uploadDeviceFirmware(dev);
            break;
        }
    }

    libusb_free_device_list(devs, 1);

    R(status);
}

static int findDeviceWithFirmware(USBDevice *dev) {
    DBG("Looking for device %04x:%04x\n", dev->usbDevVID, dev->usbDevPID);

    libusb_device **devs;
    ssize_t cnt = libusb_get_device_list(NULL, &devs);
    if (cnt < 0) {
        ERR("libusb_get_device_list failed: %s!\n", libusb_error_name(cnt));
        R(VI_ERROR_SYSTEM_ERROR);
    }

    libusb_device *usbDev;
    int i = 0;
    int status = VI_ERROR_RSRC_NFOUND;
    while ((usbDev = devs[i++]) != NULL) {
        struct libusb_device_descriptor desc;
        int r = libusb_get_device_descriptor(usbDev, &desc);
        if (r < 0) {
            ERR("libusb_get_device_descriptor failed: %s!\n", libusb_error_name(cnt));
            status = VI_ERROR_SYSTEM_ERROR;
            break;
        }

        DBG("%04x:%04x (bus %d, device %d)\n",
            desc.idVendor, desc.idProduct,
            libusb_get_bus_number(usbDev), libusb_get_device_address(usbDev));

        if ((desc.idVendor == dev->usbDevVID) && (desc.idProduct == dev->usbDevPID)) {
            INF("Found device %04x:%04x\n", dev->usbDevVID, dev->usbDevPID);
            dev->usbDev = usbDev;
            status = registerDevice(dev);
            break;
        }
    }

    libusb_free_device_list(devs, 1);

    R(status);
}

struct fwheader {
    char tag[4];                    /* tag string: CSPT */
    uint32_t msgLen;                /* length of this message w/ header */
    uint32_t hdrLen;                /* length of header 32 (0x20) bytes */
    unsigned char res00000050[4];   /* ?? always 00 00 00 50 */
    unsigned char bRequest;         /* bRequest in USB msg, always A0 */
    unsigned char res70;            /* ?? always 70 */
    uint16_t wValue;                /* wValue in USB msg */
    uint16_t wIndex;                /* wIndex in USB msg, always 00 00 */
    unsigned char res6C65[2];       /* ?? always 6C 65 */
    unsigned char res0F000000[4];   /* ?? always 0F 00 00 00 */
    uint32_t wLength;               /* wLength in USB msg */
};
#define SIZEOF_FWHEADER     (sizeof(struct fwheader))

static int uploadDeviceFirmware(USBDevice *dev) {
    int r;
    unsigned int pos;
    unsigned char *buf;
    struct fwheader *hdr;
    char model[10] = {0};
    unsigned char *data;
    unsigned int len;
    int addr, bus;

    r = openDevice(dev);
    if (r < 0) {
        return -1;
    }

    switch (dev->usbDevPID) {
    case CCS100_PID - 1:
        /* CCS100 */
        snprintf(model, 10, "CCS100");
        data = CCS100_spt;
        len = CCS100_spt_len;
        break;
    case CCS125_PID - 1:
        /* CCS125 */
        snprintf(model, 10, "CCS125");
        data = CCS125_spt;
        len  = CCS125_spt_len;
        break;
    case CCS150_PID - 1:
        /* CCS150 */
        snprintf(model, 10, "CCS150");
        data = CCS150_spt;
        len  = CCS150_spt_len;
        break;
    case CCS175_PID - 1:
        /* CCS175 */
        snprintf(model, 10, "CCS175");
        data = CCS175_spt;
        len  = CCS175_spt_len;
        break;
    case CCS200_PID - 1:
        /* CCS200 */
        snprintf(model, 10, "CCS200");
        data = CCS200_spt;
        len  = CCS200_spt_len;
        break;
    default:
        ERR("Invalid PID 0x%x\n", dev->usbDevPID);
        return -1;
        break;
    }

    addr = libusb_get_device_address(dev->usbDev);
    bus = libusb_get_bus_number(dev->usbDev);

    DBG("uploading firmware for bus %d, address %d, model %s..\n",
            bus, addr, model);

    pos = 0;
    do {
        /* get header */
        if ((pos + SIZEOF_FWHEADER) <= len) {
            hdr = (struct fwheader *)(data + pos);
        } else {
            ERR("premature end of file\n");
            return -1;
        }
        hexdump(stdout, hdr, SIZEOF_FWHEADER);
        // DBG2("TAG:         %.*s\n", 4, hdr->tag);
        // DBG2("wValue:      0x%X\n", hdr->wValue);
        // DBG2("wLength:     0x%X\n", hdr->wLength);

        pos += SIZEOF_FWHEADER;
        if ((pos + hdr->wLength) <= len) {
            buf = (data + pos);
        } else {
            ERR("premature end of file\n");
            return -1;
        }
        // DBG2("data length: 0x%X (%d)\n", r, r);
        hexdump(stdout, buf, hdr->wLength);

        r = libusb_control_transfer(
                dev->usbDevHandle,          /* handle */
                0x40,                       /* bmRequestType */
                hdr->bRequest,              /* bRequest */
                hdr->wValue,                /* wValue */
                hdr->wIndex,                /* wIndex */
                buf,                        /* data buffer */
                hdr->wLength,               /* data size */
                1000                        /* timeout */
                );
        if (r != (int)hdr->wLength) {
            ERR("failed to send all bytes; %d / %d\n", r, hdr->wLength);
            return -1;
        } else if (r < 0) {
            ERR("libusb_control_transfer failed (%d): %s\n", r, libusb_error_name(r));
            return -1;
        }

        pos += hdr->wLength;
    } while (pos < len);

    /* Last message sent to the USB device needs to be with proper wValue! */
    if (hdr->wValue != 0xE600) {
        ERR("invalid last command 0x%X; should be 0xE600\n", hdr->wValue);
        return -1;
    }
    DBG("firmware upload done!\n");

    closeDevice(dev);

    return 0;
}

static int registerDevice(USBDevice *dev) {
    int r = openDevice(dev);
    if (r) {
        return r;
    }
    r = getDeviceStrings(dev);
    if (r) {
        return r;
    }
    char buf[VI_FIND_BUFLEN];
    // Resource string: USB::VID::PID::SERIAL::RAW
    // Ex: USB::0x1313::0x8089::M00414547::RAW
    snprintf(buf, VI_FIND_BUFLEN, "USB::0x%04X::0x%04X::%s::RAW",
                dev->usbDevVID, dev->usbDevPID, dev->usbSerialNum);
    DBG("comparing device RSRC '%s' with user RSRC '%s'\n", buf, dev->rsrc);
    if (strncmp(dev->rsrc, buf, VI_FIND_BUFLEN) != 0) {
        closeDevice(dev);
        return VI_ERROR_RSRC_NFOUND;
    }
    DBG("matched device RSRC '%s' with user RSRC '%s'\n", buf, dev->rsrc);
    // we have a match, is the device free?
    if (! isDeviceFree(dev)) {
        ERR("Device with RSRC '%s' is not free!", buf);
        closeDevice(dev);
        return VI_ERROR_RSRC_LOCKED;
    }

    ViUInt32 hash = RSHash(buf, strlen(buf));
    dev->viSession = hash;
    DBG("device '%s' session 0x%08X\n", buf, dev->viSession);
    dev->manfID = dev->usbDevVID;
    dev->modelCode = dev->usbDevPID;
    dev->usbDevBulkInEndpoint = 0x86;
    
    return VI_SUCCESS;
}

static int unregisterDevice(USBDevice *dev) {
    closeDevice(dev);
    memset(dev, 0, sizeof(USBDevice));
    return VI_SUCCESS;
}

static int openDevice(USBDevice *dev) {
    libusb_device_handle *handle = NULL;
    int r = libusb_open(dev->usbDev, &handle);
    if (r) {
        ERR("libusb_open failed: %s!\n", libusb_error_name(r));
        return -1;
    }
    DBG("opened USB device..\n");

    r = libusb_kernel_driver_active(handle, 0);
    if (r == 1) {
        DBG("kernel driver is active for interface 0\n");
        r = libusb_detach_kernel_driver(handle, 0);
        if (r) {
            ERR("libusb_detach_kernel_driver failed: %s\n", libusb_error_name(r));
            return -1;
        }
    } else if (r == 0) {
        DBG("kernel driver is NOT active for interface 0\n");
    } else {
        ERR("libusb_kernel_driver_active failed: %s\n", libusb_error_name(r));
        return -1;
    }

    int config = 0;
    r = libusb_get_configuration(handle, &config);
    if (r) {
        ERR("libusb_get_configuration failed: %s\n", libusb_error_name(r));
        return -1;
    }
    DBG("current configuration is %d\n", config);

    r = libusb_claim_interface(handle, 0);
    if (r) {
        ERR("libusb_claim_interface failed: %s\n", libusb_error_name(r));
        return -1;
    }
    DBG("claimed interface 0\n");
    DBG("handle %p\n", handle);

    dev->usbDevHandle = handle;
    dev->usbDevClaimed = 1;

    return 0;
}

static int closeDevice(USBDevice *dev) {
    if (! dev->usbDevHandle) {
        DBG("USB device not opened\n");
        return 0;
    }

    if (dev->usbDevClaimed) {
        int r = libusb_release_interface(dev->usbDevHandle, 0);
        if (r) {
            ERR("libusb_release_interface() failed: %s\n", libusb_error_name(r));
        }
        DBG("released interface 0\n");
    }
    DBG("handle %p\n", dev->usbDevHandle);

    libusb_close(dev->usbDevHandle);
    dev->usbDevHandle = NULL;
    dev->usbDevClaimed = 0;

    DBG("closed USB device\n");

    return 0;
}

static int getDeviceStrings(USBDevice *dev) {
    struct libusb_device_descriptor desc;
    int r = libusb_get_device_descriptor(dev->usbDev, &desc);
    if (r) {
        ERR("libusb_get_device_descriptor failed: %s\n", libusb_error_name(r));
        return -1;
    }

    char buf[VI_FIND_BUFLEN] = {0};
    if (desc.iManufacturer) {
        r = libusb_get_string_descriptor_ascii(dev->usbDevHandle, desc.iManufacturer,
                (unsigned char *)buf, VI_FIND_BUFLEN);
        if (r < 0) {
            ERR("libusb_get_string_descriptor_ascii failed: %s\n", libusb_error_name(r));
            return -1;
        }
        strncpy(dev->manfName, buf, VI_FIND_BUFLEN);
    }

    if (desc.iProduct) {
        r = libusb_get_string_descriptor_ascii(dev->usbDevHandle, desc.iProduct,
                (unsigned char *)buf, VI_FIND_BUFLEN);
        if (r < 0) {
            ERR("libusb_get_string_descriptor_ascii failed: %s\n", libusb_error_name(r));
            return -1;
        }
        strncpy(dev->modelName, buf, VI_FIND_BUFLEN);
    }
    if (desc.iSerialNumber) {
        r = libusb_get_string_descriptor_ascii(dev->usbDevHandle, desc.iSerialNumber,
                (unsigned char *)buf, VI_FIND_BUFLEN);
        if (r < 0) {
            ERR("libusb_get_string_descriptor_ascii failed: %s\n", libusb_error_name(r));
            return -1;
        }
        strncpy(dev->usbSerialNum, buf, VI_FIND_BUFLEN);
    }

    return 0;
}


#ifdef _DBG
static void hexdump(FILE * stream, void const * data, unsigned int len) {
    unsigned int i;
    unsigned int r, c;

    if (!stream)
        return;
    if (!data)
        return;

    for (r = 0, i = 0; r < (len / 16 + (len % 16 != 0)); r++, i += 16) {
        fprintf(stream, "%04X:   ", i); /* location of first byte in line */

        for (c = i; c < i + 8; c++) /* left half of hex dump */
            if (c < len)
                fprintf(stream, "%02X ", ((unsigned char const *) data)[c]);
            else
                fprintf(stream, "   "); /* pad if short line */

        fprintf(stream, "  ");

        for (c = i + 8; c < i + 16; c++) /* right half of hex dump */
            if (c < len)
                fprintf(stream, "%02X ", ((unsigned char const *) data)[c]);
            else
                fprintf(stream, "   "); /* pad if short line */

        fprintf(stream, "   ");

        for (c = i; c < i + 16; c++) /* ASCII dump */
            if (c < len)
                if (((unsigned char const *) data)[c] >= 32
                        && ((unsigned char const *) data)[c] < 127)
                    fprintf(stream, "%c", ((char const *) data)[c]);
                else
                    fprintf(stream, "."); /* put this for non-printables */
            else
                fprintf(stream, " "); /* pad if short line */

        fprintf(stream, "\n");
    }

    fflush(stream);
}
#else
static void hexdump(FILE * stream, void const * data, unsigned int len) {}
#endif

static int xferDeviceControl(USBDevice *dev,
        uint8_t bmRequestType,
        uint8_t bRequest,
        uint16_t wValue,
        uint16_t wIndex,
        unsigned char *data,
        uint16_t wLength,
        unsigned int timeout) {

    ASSERT(dev != NULL);

    int r = libusb_control_transfer(
            dev->usbDevHandle,          /* handle */
            bmRequestType,              /* bmRequestType */
            bRequest,                   /* bRequest */
            wValue,                     /* wValue */
            wIndex,                     /* wIndex */
            data,                       /* data buffer */
            wLength,                    /* data size */
            timeout                     /* timeout */
            );
    if (r < 0) {
        ERR("libusb_control_transfer failed: %s\n", libusb_error_name(r));
    }
    // hexdump(stdout, data, wLength);

    return r;
}

static int xferDeviceBulk(USBDevice *dev,
        unsigned char *data,
        int wLength,
        int *wActualLength,
        unsigned int timeout) {

    ASSERT(dev != NULL);

    int r = libusb_bulk_transfer(
            dev->usbDevHandle,          /* handle */
            dev->usbDevBulkInEndpoint,  /* endpoint */
            data,                       /* data buffer */
            wLength,                    /* wLength */
            wActualLength,              /* wActualLength */
            timeout                     /* timeout */
            );
    if (r < 0) {
        ERR("libusb_bulk_transfer failed: %s\n", libusb_error_name(r));
    }
    // hexdump(stdout, data, wLength);

    return r;
}

/*
 From http://partow.net/programming/hashfunctions/index.html#top
 */
static ViUInt32 RSHash(const ViPChar str, ViUInt32 length)
{
   unsigned int b    = 378551;
   unsigned int a    = 63689;
   unsigned int hash = 0;
   unsigned int i    = 0;
   char *s           = str;

   for (i = 0; i < length; ++s, ++i)
   {
      hash = hash * a + (*s);
      a    = a * b;
   }

   return hash;
}
